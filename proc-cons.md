# Оцінка міграції з Bitbucket server та Jenkins у Gitlab SaS

## Плюси міграції

1. Функціональність: Gitlab SaS має широкій набір можливостей та функцій для атоматизації тестування, моніторингу, розгортання.
2. Простота: У разі використання сховища артефактів у Gitlab, Gitlab SaS дозволяє зберігати їх прямо у джобах, що дозволяє уникнути стейджів с авторизацією до сховищ, крім того - використання Gitlab як єдиного джерела коду, також прибирається необхідність авторизації до репозиторію, на відміну від Jenkins.
3. Економія коштів: Gitlab SaS певно надає більше фукнціоналу, більшість з якого є безкоштовним.
4. Безпека: Gitlab SaS є більш безпечним, ніж Bitbucket server.
5. Ранери та масштабування: Gitlab SaS дозволяє побудувати широку мережу з виконання різних задач CI/CD на ремоут серверах.
6. Зручність: Єдине платформа Gitlab як джерела коду, зрерігання артефактів та моніторингу задач CI/CD, що може бути набагато гнучкіше і зручніше для команди розробників.

## Мінуси міграції

6. Витрати часу: Міграція може призвести до значних втрат часу. 
7. Складність: Міграція може бути непростою задачею, якщо пайплайн складний, має багато кроків, а також якщо використовувались Jenkins плагіни або сторонні бібліотеки.